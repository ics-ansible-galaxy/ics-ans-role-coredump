import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_files(host):
    file1 = host.file("/etc/security/limits.conf")
    assert file1.contains("* hard core 0")
    file2 = host.file("/etc/sysctl.conf")
    assert file2.contains("fs.suid_dumpable=0")
    file3 = host.file("/etc/profile")
    assert file3.contains("ulimit -S -c 0 > /dev/null 2>&1")
