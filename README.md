# ics-ans-role-coredump

disable coredumps.

## Role Variables

```yaml
...
Default is to disable core dumps.

# present|absent
disable_coredump: present
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-coredump
```

## License

BSD 2-clause
